package com.develops.phonearena;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.develops.graphlibrary.Graph;
import com.develops.graphlibrary.Hasil;
import com.develops.graphlibrary.Watermark;

import java.util.ArrayList;
import java.util.LinkedList;

public class About extends AppCompatActivity {

    private static final String TAG = "About";

    private ArrayList<Character> mark = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        watermark();

    }
    private void watermark() {

        TextView ascii = findViewById(R.id.n_ascii);
        LinkedList<Integer> adj[];
        Graph g = new Graph(100);

        Watermark kata= new Watermark("Thequickbrownfox");

        String kata_watermark = kata.encodeString(kata.nama_watermark);

        //System.out.println(kata_watermark);

        Hasil hasil = new Hasil();

        Hasil.buildWatermark(kata_watermark);

        adj = g.getAdjacencyList();

        int panjangAdjencyList = Hasil.getAdjecencyListSize();

        String watermark = hasil.getASCIIFromAdjacencyList(adj, panjangAdjencyList);

        System.out.println(watermark);

        String decodeWatermark = kata.decodeString(watermark);

        System.out.println("d "+decodeWatermark);

        ascii.setText(decodeWatermark);
    }

}
