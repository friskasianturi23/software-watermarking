package com.develops.graphlibrary;

import android.util.Pair;

import java.util.ArrayList;
import java.util.LinkedList;

public class Hasil {
    public static ArrayList <Pair <Integer,Integer> > data = new ArrayList <Pair <Integer,Integer> > ();
    static Graph g= new Graph(100);
    public static int counterhuruf = 0;

    public static void buildWatermark(String watermark){

        int ctr=2;
        for(char c: watermark.toCharArray()){
            counterhuruf++;
            int character_angka = Character.getNumericValue(c);
            for(int i = counterhuruf; i < counterhuruf + character_angka ;i++){
                g.addEdge(counterhuruf, ctr);
                ctr++;
            }

        }

    }
    public static int getAdjecencyListSize(){
        return counterhuruf+1;
    }
    public String getASCIIFromAdjacencyList(LinkedList<Integer> adj[], int limitpanjanglist){
        String watermark = "";
        for(int i = 0 ; i < limitpanjanglist+1 ; i++)
        {
            if(adj[i].size() > 0)
            {
                watermark+= Integer.toString(adj[i].size());
            }

        }
        return watermark;
    }

}