package com.develops.graphlibrary;

public class Watermark {
    public String nama_watermark;
    public Watermark(String watermark){
        nama_watermark = watermark;
    }
    public String encodeString(String watermark ){
        String watermarkAscii="";
        for (char c : watermark.toCharArray()){
            watermarkAscii +=Integer.toString(cekKodeEncode((int)c));
            //System.out.println(c + " " + watermarkAscii);
        }
        return watermarkAscii;
    }
    private int cekKodeEncode(int ascii){
        if(encodeAsciiSeratus(ascii)){
            ascii = 138;
        }else if(encodeAsciiLebihSeratus(ascii)){
            ascii+=10;
        }else if(encodeAsciiDuaAngka(ascii)){
            ascii+=200;
        }else if(encodeAsciiHabisModSepuluh(ascii)){
            ascii+=301;
        }
        return ascii;
    }
    private boolean encodeAsciiSeratus(int ascii){
        if(ascii == 100){
            return true;
        }
        return false;
    }
    private boolean encodeAsciiLebihSeratus(int ascii){
        if(ascii > 100 && ascii <200 && ascii %10 !=0){
            return true;
        }
        return false;
    }
    private boolean encodeAsciiDuaAngka(int ascii){
        if(ascii < 100 &&  ascii % 10 != 0){
            return true;
        }
        return false;
    }
    private boolean encodeAsciiHabisModSepuluh(int ascii){
        if(ascii % 10 == 0){
            return true;
        }
        return false;
    }

    public String decodeString(String watermark ){
        String watermarkAscii ="";
        int size = 3;
      //  String[] tokens = watermark.split("(?<=\\G.{" + size + "})");
        for(int i = 0 ;i < watermark.length() ;i+=size){
            String subkata = watermark.substring(i, Math.min(watermark.length(), i + size));
            System.out.println(i + " "+ i+size +" " +subkata);
            int kataASCII = cekKodeDecode(Integer.parseInt(subkata));
            watermarkAscii +=   (char)kataASCII;
        }

        return watermarkAscii;
    }
    public int cekKodeDecode(int ascii){
        if(decodeAsciiSeratus(ascii)){
            ascii = 100;
        }else if(decodeAsciiLebihSeratus(ascii)){
            ascii-=10;
        }else if(decodeAsciiDuaAngka(ascii)){
            ascii-=200;
        }else if(decodeAsciiHabisModSepuluh(ascii)){
            ascii-=301;
        }
        return ascii;
    }
    private boolean decodeAsciiSeratus(int ascii){
        if(ascii == 138){
            return true;
        }
        return false;
    }
    private boolean decodeAsciiLebihSeratus(int ascii){
        if(ascii > 100 && ascii <200 && ascii %10 !=0){
            return true;
        }
        return false;
    }
    private boolean decodeAsciiDuaAngka(int ascii){
        if(ascii > 200 &&  ascii < 300){
            return true;
        }
        return false;
    }
    private boolean decodeAsciiHabisModSepuluh(int ascii){
        if(ascii > 300){
            return true;
        }
        return false;
    }
}
